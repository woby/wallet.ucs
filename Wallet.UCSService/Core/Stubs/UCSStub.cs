﻿using System;
using System.Collections.Generic;
using System.Linq;
using Wallet.UCSService.Core.Interfaces;
using Wallet.UCSService.Core.Obj;

namespace Wallet.UCSService.Core.Stubs
{
    public class UCSStub : IUCSService, IInvoicingService, IStorage
    {
        public static UCSStub Instance
        {
            get { return _instance; }
        }
        private static UCSStub _instance;
        static UCSStub()
        {
            _instance = new UCSStub();
            _instance.CurrentOrder = new UcsOrder
                {
                    OrderState = UcsOrder.OrderState_Unknown,
                    StartService = DateTime.UtcNow,
                    Items = new List<UcsOrderItem>(
                            (new[] {new UcsOrderItem {Name = "TestItem", Price = 1, Quantity = 1, Summ = 1}}).ToList())
                };
            
            _instance.Requests = new Dictionary<string, DateTime>();
        }
        private UCSStub(){}

        private object _synObj = new object();
        private UcsOrder _currentOrder;
        protected Dictionary<string, DateTime> Requests;

        public UcsOrder CurrentOrder
        {
            get
            {
                lock (_synObj)
                {
                    return (UcsOrder)_currentOrder.Clone();
                }
            }

            set
            {
                if(value == null)
                    return;
                
                decimal total = 0;
                if (value.Items != null)
                    foreach (var item in value.Items)
                    {
                        if (!string.IsNullOrEmpty(item.Name))
                        {
                            item.Summ = item.Price * item.Quantity;
                            total += item.Summ;
                        }
                    }
                value.CurrencyId = 643;
                value.Amount = total;
                value.TotalCommission = total * (decimal)0.05;
                value.TotalAmount = value.Amount + value.TotalCommission;

                lock (_synObj)
                {
                    _currentOrder = (UcsOrder)value.Clone();
                }
            }
        }
        

        //IUCSService
        public void RequestOrderInfo(string qrid)
        {
            lock (_synObj)
            {
                if (!Requests.ContainsKey(qrid))
                    Requests.Add(qrid, DateTime.UtcNow);

                _currentOrder.QrId = qrid;
            }
        }

        //IInvoicingService
        public Invoice CreateInvoiceByQr(UcsOrder ucsOrder, MerchantInfo merchant)
        {
            return new InvoicingApiHelper(merchant.MerchId, merchant.MerchKey).CreateInvoice("test", ucsOrder.QrId, ucsOrder.TotalAmount);
        }

        //IStorage
        public MerchantInfo ResolveMerchant(string ucsId)
        {
            return new MerchantInfo
                {
                    MerchId = "121420533335",
                    MerchKey = "5b713049426d5334303947665d7346563661603960525b77386845"
                };
        }
        public UcsOrder GetOrder(string qrId)
        {
            lock (_synObj)
            {
                if (Requests.ContainsKey(qrId))
                {
                    if ((DateTime.UtcNow - Requests[qrId]).Seconds < 3)
                        return new UcsOrder { OrderState = UcsOrder.OrderState_InfoRequested };


                    _currentOrder.OrderState = UcsOrder.OrderState_WaitPayment;
                    
                    var retval = (UcsOrder)_currentOrder.Clone();
                    retval.QrId = qrId;
                    return retval;
                }

                return null;
            }
        }
        public void StoreOrder(UcsOrder order)
        {
            lock (_synObj)
            {
                var tmp = CurrentOrder;

                tmp.QrId = order.QrId;
                tmp.MerchantId = order.MerchantId;
                tmp.MerchantLogoUrl = order.MerchantLogoUrl;
                tmp.MerchantName = order.MerchantName;
                tmp.OrderState = order.OrderState;

                CurrentOrder = tmp;
            }
        }
    }
}