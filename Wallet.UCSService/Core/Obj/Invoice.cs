﻿namespace Wallet.UCSService.Core.Obj
{
    public class Invoice
    {
        public string QrId { get; set; }
        public string InvoiceId { get; set; }
        public string InvoiceStateId { get; set; }
    }
}