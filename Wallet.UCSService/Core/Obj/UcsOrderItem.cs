﻿using System;

namespace Wallet.UCSService.Core.Obj
{
    public class UcsOrderItem : ICloneable
    {
        public string Name { get; set; }
        public int Quantity { get; set; }
        public decimal Price { get; set; }
        public decimal Summ { get; set; }

        public object Clone()
        {
            return new UcsOrderItem
                {
                    Name = this.Name,
                    Quantity = this.Quantity,
                    Price = this.Price,
                    Summ = this.Summ
                };
        }
    }
}