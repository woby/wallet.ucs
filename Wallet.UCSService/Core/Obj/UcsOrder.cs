﻿using System;
using System.Collections.Generic;


namespace Wallet.UCSService.Core.Obj
{
    public class UcsOrder: ICloneable
    {
        public const string OrderState_Unknown = "unknown";
        public const string OrderState_InfoRequested = "infoRequested";
        public const string OrderState_WaitPayment = "waitPayment";
        public const string OrderState_PaymentConfirmation = "paymentConfirmation";
        public const string OrderState_Ok = "success";
        public const string OrderState_Error = "error";
        
        
        public string QrId { get; set; }
        public string OrderState { get; set; }
        public string MerchantId { get; set; }
        public string MerchantName { get; set; }
        public string MerchantLogoUrl { get; set; }
        public DateTime StartService { get; set; }
        public int CurrencyId { get; set; }
        public decimal Amount { get; set; }
        public decimal TotalCommission { get; set; }
        public decimal TotalAmount { get; set; }
        public List<UcsOrderItem> Items { get; set; }
        public string ErrorDescription { get; set; }


        public object Clone()
        {
            var retval = new UcsOrder
                {
                    QrId = QrId,
                    OrderState = OrderState,
                    StartService = StartService,
                    ErrorDescription = ErrorDescription,
                    CurrencyId = CurrencyId,
                    Amount = Amount,
                    TotalCommission = TotalCommission,
                    TotalAmount = TotalAmount,
                    MerchantId = MerchantId,
                    MerchantName = MerchantName,
                    MerchantLogoUrl = MerchantLogoUrl
                };

            var list = new List<UcsOrderItem>();

            if (Items != null)
            {
                foreach (var item in Items)
                list.Add((UcsOrderItem) item.Clone());
            }
            retval.Items = list;

            return retval;
        }
    }
}