﻿using Wallet.UCSService.Core.Obj;

namespace Wallet.UCSService.Core.Interfaces
{
    public interface IInvoicingService
    {
        Invoice CreateInvoiceByQr(UcsOrder ucsOrder, MerchantInfo merchant);
    }
}