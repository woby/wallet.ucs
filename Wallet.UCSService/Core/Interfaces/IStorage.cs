﻿using Wallet.UCSService.Core.Obj;

namespace Wallet.UCSService.Core.Interfaces
{
    public interface IStorage
    {
        MerchantInfo ResolveMerchant(string ucsId);
        UcsOrder GetOrder(string qrId);
        void StoreOrder(UcsOrder order);
    }
}