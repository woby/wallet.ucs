﻿namespace Wallet.UCSService.Core.Interfaces
{
    public interface IUCSService
    {
        void RequestOrderInfo(string qrid);
    }
}