﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Wallet.UCSService.Core.Obj;

namespace Wallet.UCSService.Core
{
    public class InvoicingApiHelper
    {
        private string _merchId;
        private string _key;
        //private const string _apiUrl = @"http://localhost:6245/checkout/invoicingapi/invoices";
        private const string _apiUrl = @"https://wl.walletone.com/checkout/invoicingapi/invoices";
        private const string _paymentTypeId = "CreditCardRub";
        private const int _currencyId = 643;

        private const string HeaderUserId = "X-Wallet-UserId";
        private const string HeaderTimestamp = "X-Wallet-Timestamp";
        private const string HeaderSignature = "X-Wallet-Signature";

        public InvoicingApiHelper(string merchId, string key)
        {
            _merchId = merchId;
            _key = key;
        }

        public Invoice CreateInvoice(string userId, string qrId, decimal amount)
        {
            Invoice retVal;
            try
            {
                var requestText = new CreateInvoiceRequest
                    {
                        Amount = amount,
                        CurrencyId = _currencyId,
                        OrderId = qrId,
                        PaymentTypeId = _paymentTypeId,
                        MerchantUserId = userId
                    }.ToJson();

                var timestamp = DateTime.UtcNow.ToString("yyyy-MM-ddTHH:mm:ss");

                //string signatureLine = string.Format("{0}{1}{2}{3}{4}{5}", _apiUrl, _merchId, userId, timestamp, requestText, _key);
                string signatureLine = string.Format("{0}{1}{2}{3}{4}", _apiUrl, _merchId, timestamp, requestText, _key);
                byte[] ignatureBytes = Encoding.UTF8.GetBytes(signatureLine);
                string signature = Convert.ToBase64String(MD5.Create().ComputeHash(ignatureBytes));

                byte[] requestBytes = Encoding.UTF8.GetBytes(requestText);

                var request = (HttpWebRequest) WebRequest.Create(_apiUrl);
                request.ContentType = "text/json; charset=UTF-8";
                request.Method = WebRequestMethods.Http.Post;
                request.ContentLength = requestBytes.Length;

                request.Headers.Add(HeaderUserId, _merchId);
                request.Headers.Add(HeaderTimestamp, timestamp);
                request.Headers.Add(HeaderSignature, signature);

                using (var stream = request.GetRequestStream())
                    stream.Write(requestBytes, 0, requestBytes.Length);

                string respText = string.Empty;
                var response = request.GetResponse();
                using (var stream = response.GetResponseStream())
                    respText = new StreamReader(stream, Encoding.UTF8).ReadToEnd();

                var jsonResp = JObject.Parse(respText);
                
                var jInvoice = jsonResp["Invoice"];
                retVal = new Invoice
                    {
                        InvoiceId = jInvoice["InvoiceId"].ToString(),
                        InvoiceStateId = jInvoice["InvoiceStateId"].ToString()
                    };
            }
            catch (Exception ex)
            {
                retVal = new Invoice
                {
                    InvoiceId = "",
                    InvoiceStateId = "error"
                };
            }

            retVal.QrId = qrId;
            return retVal;
        }

        private class CreateInvoiceRequest
        {
            public decimal Amount { get; set; }
            public int CurrencyId { get; set; }
            public string PaymentTypeId { get; set; }
            public string OrderId { get; set; }
            public string MerchantUserId { get; set; }
            //public Dictionary<string, string> InvoiceAdditionalParams { get; set; }

            public string ToJson()
            {
                return JsonConvert.SerializeObject(this, Formatting.None, new JsonSerializerSettings
                    {
                        Converters = new List<JsonConverter>
                            {
                                new Newtonsoft.Json.Converters.StringEnumConverter()
                            }
                    });
            }
        }
    }
}
