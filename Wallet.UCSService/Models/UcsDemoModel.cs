﻿using Wallet.UCSService.Core.Obj;

namespace Wallet.UCSService.Models
{
    public class UcsDemoModel
    {
        public UcsOrder Order { get; set; }

        public string Action { get; set; }
    }
}