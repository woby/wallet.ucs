﻿namespace Wallet.UCSService.Models
{
    public class W1InvoiseNotificationModel
    {
        public string WMI_MERCHANT_ID { get; set; }
        public string WMI_PAYMENT_AMOUNT { get; set; }
        public string WMI_CURRENCY_ID { get; set; }
        public string WMI_PAYMENT_NO { get; set; }
        public string WMI_ORDER_ID { get; set; }
        public string WMI_ORDER_STATE { get; set; }
        public string WMI_SIGNATURE { get; set; }
    }
}