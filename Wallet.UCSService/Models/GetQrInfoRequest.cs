﻿namespace Wallet.UCSService.Models
{
    public class GetQrInfoRequest
    {
        public string TerminalId { get; set; }
        public string QRId { get; set; }
    }
}