﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Wallet.UCSService.Models
{
    public class CreateQrInvoiceRequest
    {
        public string QRId { get; set; }
    }
}