﻿using System.Web.Mvc;
using System.Web.Routing;

namespace Wallet.UCSService
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "UcsDemo",
                url: "UcsDemo/{action}",
                defaults: new { controller = "UcsDemo", action = "Index" }
            );
        }
    }
}