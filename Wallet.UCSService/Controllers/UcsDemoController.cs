﻿using System.Web.Mvc;
using Wallet.UCSService.Core.Obj;
using Wallet.UCSService.Core.Stubs;
using Wallet.UCSService.Models;

namespace Wallet.UCSService.Controllers
{
    public class UcsDemoController : Controller
    {
        [HttpGet]
        public ActionResult Index()
        {
            var model = new UcsDemoModel();
            model.Order = UCSStub.Instance.CurrentOrder;
            return View(model);
        }

        [HttpPost]
        public ActionResult Index(UcsDemoModel model)
        {
            if (model != null && model.Order != null)
            {
                switch (model.Action)
                {
                    case "refresh":
                        model.Order = UCSStub.Instance.CurrentOrder;
                        break;


                    case "update":
                        var tmp = UCSStub.Instance.CurrentOrder;
                        tmp.Items = model.Order.Items;
                        UCSStub.Instance.CurrentOrder = tmp;
                        break;


                    case "make_paid":
                        //????
                        break;

                    case "make_err":
                        model.Order.OrderState = UcsOrder.OrderState_Error;
                        model.Order.ErrorDescription = "Error text is here";
                        UCSStub.Instance.CurrentOrder = model.Order;
                        break;
                }
            }

            return View(model);
        }

    }
}
