﻿using System;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Mvc;
using Newtonsoft.Json;
using Wallet.UCSService.Core;
using Wallet.UCSService.Core.Interfaces;
using Wallet.UCSService.Core.Stubs;
using Wallet.UCSService.Models;
using System.Net.Http.Headers;
using Wallet.UCSService.Core.Obj;

namespace Wallet.UCSService.Controllers
{
    public class UcsApiController : ApiController
    {
        protected IInvoicingService InvoicingService;
        protected IStorage Storage;
        protected IUCSService UcsService;


        public UcsApiController()
        {
            var stub = UCSStub.Instance;
            
            InvoicingService = stub;
            Storage = stub;
            UcsService = stub;

        }

        [System.Web.Http.HttpPost]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage GetQrInfo(GetQrInfoRequest request)
        {
            if (request == null)
                return CreateResponse(new UcsOrder {OrderState = UcsOrder.OrderState_Error}, HttpStatusCode.NotFound);

            var merch = Storage.ResolveMerchant(request.TerminalId);
            //validate request here

            var order = Storage.GetOrder(request.QRId);
            if (order == null)
            {
                order = new UcsOrder
                    {
                        OrderState = UcsOrder.OrderState_InfoRequested, 
                        QrId = request.QRId,
                        MerchantId = "119973638979",
                        MerchantName = "Walletone",
                        MerchantLogoUrl = @"http://www.w1.ru/static/upload/8c38f29eb0e14418867af8af0036e5ce.png"
                    };
                Storage.StoreOrder(order);
                UcsService.RequestOrderInfo(request.QRId);
            }
            
            return CreateResponse(order);
        }


        [System.Web.Http.HttpPost]
        [System.Web.Http.HttpGet]
        public HttpResponseMessage CreateQrInvoice(CreateQrInvoiceRequest request)
        {
            if (request == null)
                return CreateResponse(new Invoice() { InvoiceStateId = "error"}, HttpStatusCode.NotFound);

            var terminalId = "";//get from qr

            var merch = Storage.ResolveMerchant(terminalId);
            //validate request here

            var order = Storage.GetOrder(request.QRId);
            if (order == null)
                return CreateResponse(new Invoice() { InvoiceStateId = "error" }, HttpStatusCode.NotFound);

            var invoice = InvoicingService.CreateInvoiceByQr(order, merch);
            
            return CreateResponse(invoice);
        }


        [System.Web.Http.HttpPost]
        public HttpResponseMessage InvoiceState([FromBody]W1InvoiseNotificationModel data)
        {

            var log = new StringBuilder();
            log.AppendFormat("\n\n{0}\n", DateTime.UtcNow.ToString("yyyy-MM-dd HH:mm:ss"));
            
            //if (data != null)
            //{
            //    foreach (var k in data.AllKeys)
            //        log.AppendFormat("{0} = {1}\n", k, data[k]);
            //}

            if (data != null)
            {
                log.AppendFormat("WMI_ORDER_ID = {0}\n", data.WMI_ORDER_ID);
                log.AppendFormat("WMI_MERCHANT_ID = {0}\n", data.WMI_MERCHANT_ID);
                log.AppendFormat("WMI_PAYMENT_NO = {0}\n", data.WMI_PAYMENT_NO);
                log.AppendFormat("WMI_ORDER_STATE = {0}\n", data.WMI_ORDER_STATE);

                var order = Storage.GetOrder(data.WMI_PAYMENT_NO);
                if (order != null)
                {
                    order.OrderState = UcsOrder.OrderState_Ok;
                    Storage.StoreOrder(order);
                }
            }

            File.AppendAllText(@"c:\kill\log\out.txt", log.ToString());

            

            return new HttpResponseMessage(HttpStatusCode.OK) {Content = new StringContent("WMI_RESULT=OK", Encoding.UTF8)};
            //return CreateResponse(null);
        }


        protected HttpResponseMessage CreateResponse(object responseObj, HttpStatusCode code = HttpStatusCode.OK)
        {
            var response = new HttpResponseMessage(code);
            string content = string.Empty;
            
            if (responseObj != null)
                content = JsonConvert.SerializeObject(responseObj, new JsonSerializerSettings { NullValueHandling = NullValueHandling.Ignore });
            
            response.Content = new StringContent(content);
            response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            return response;
        }
    }
}
